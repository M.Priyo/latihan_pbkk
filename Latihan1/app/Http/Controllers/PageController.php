<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{

    function index(){
        return view('welcome');
    }
    function profile(){
        return view('profile021190031');
    }

    function news(){
        return view('news021190031');
    }

    function product(){
        return view('product021190031');
    }

    function travel(){
        return view('travel021190031');
    }

    function food(){
        return view('food021190031');
    }
    function hitung(){
        $angka = 85;
 
        return view('hitung')->with(['nilai' => $angka]);
     }
 }