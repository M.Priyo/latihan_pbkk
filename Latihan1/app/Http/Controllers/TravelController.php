<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Travel;

class TravelController extends Controller
{
    public function index(){
        $data = Travel::get();
        return view('travel.index',['data' => $data]);
   }
   }
