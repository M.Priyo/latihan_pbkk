<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\news;

class NewsController extends Controller
{
    public function index(){
        $data = news::get();
        return view('news.index',['data' => $data]);
    }
    public function create(){
        $data = News::get();
        return view('news.create',['data' => $data]);
    }
    
    public function save(Request $request){
        $news = new News();
        $news->title = $request->title; 
        $news->description = $request->description; 
        $news->views = 0;
        $news->save();
        return redirect('/news');
    }
    
    public function delete($id){
        $news = News::where('id',$id)->delete();
        return redirect('/news');
    }
    public function edit($id){
        $news = News::where('id',$id)->first();
        return view('news.edit',['news' => $news]);
    }
    public function update(Request $request){
        $news = News::where('id',$request->id)->update(
            [
                'title' => $request->title,
                'description' => $request->description,
                ]
            ); 
            return redirect('/news');
        }
        public function detail($id){
            $news = News::where('id',$id)->first();
            $update = News::where('id',$id)->update([ 'views' => $news->views+1 ]);
            return view('news.detail',['news' => $news]);
    }
}
    