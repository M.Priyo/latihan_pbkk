@extends('layouts.app')
@section('title','halaman news')
@section('main')

<div class="container">
<div class="row mt-3 mb-3">
    <form action="{{url('/news/create/') }}" method="post">
        @csrf
        <div class="mb-3">
            <label> Title </label>
            <input type="text" class="form-control" name="title">
        </div>
        
        <div class="mb-3">
            <label> Description </label>
            <textarea name="description" class="form-control"></textarea>
        </div>
        
        <div class="mb-3">
        <button class="btn btn-primary">SUBMIT</button>
        </div>
</div>
</div>

@endsection