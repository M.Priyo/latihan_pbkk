@extends('layouts.app')
@section('title','halaman news')
@section('main')

<div class="container">
    <div class="row mt-3 mb-3">
        <div class="col-mb-10 offset-mb-1 mb-3">
            <h2>{{ $news->title }}</h2>
            {{ $news->description }}
        </div>
    </div>
</div>

@endsection